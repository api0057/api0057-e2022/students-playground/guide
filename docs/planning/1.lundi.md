# Lundi

## Introduction

!> **🕑 9h -> 9h30**

- Présentation de l'API
- Présentation des intervenants
- Pensez à lever les yeux de l'écran au moins toutes les 20 minutes 😳
- Et c'est parti ! 🔥

## Bonnes pratiques de dev (partie 1)

!> **🕑 Toute la matinée 9h30 -> 13h et en début d'aprem 14h30 -> 15h30**

**Objectifs pédagogiques :**

- [ ] Comprendre ce qu'est `ssh` et savoir l'utiliser pour se connecter à des machines distantes et faire un tunnel SSH
- [ ] Savoir naviguer sur l'interface d'un fournisseur cloud ([Scaleway](https://console.scaleway.com/)) et déployer une VM
- [ ] Savoir configurer sa shell et en comprendre l'intérêt en terme d'efficacité
- [ ] Connaître les principales commandes `git` et savoir les utiliser

### Se connecter à une machine distante avec `SSH` Secure Shell

!> **🕑 Durée indicative : 30 minutes**

> [!QUESTION]
> Qu'est-ce que `ssh` ? Quels sont ses usages ? Quelles différences entre `ssh` et `telnet` (par exemple) ?

- Savoir générer une clef SSH

```bash
ssh-keygen
# Lisez attentivement ce que le prompt vous indique...
```

- Savoir générer une clef SSH avec une passphrase

> [!QUESTION]
> À quoi sa sert la passphrase ?

> [!EXERCISE]
> L'objectif est ici d'apprendre à utiliser le protocol `ssh` pour se connecter à une machine distante.
>
> - Se connecter en `ssh` à une machine distante
>
> ```bash
> ssh user@XXX.XXX.XXX.XXX
> # Remplacer par l'utilisateur de la machine distante et l'IP par l'IP de la machine à récupérer sur Scalewway...
> ```

> [!BONUS]
>
> `man ssh` est votre ami...
>
> - Savoir faire un tunnel SSH avec forwarding de port
>
> ```bash
> ssh -L [local_ip:]local_port:destination:destination_port user@XXX.XXX.XXX.XXX
> ```
>
> > Quelle est la différence entre les options `-L` et `-D` ?

> [!BONUS]
>
> - Savoir les bases de la configuration dans son `.ssh/config`
>
> * Se connecter en `ssh` à une machine distante en utilisant du "Agent Forwarding"
>
> ```bash
> ssh -A user@XXX.XXX.XXX.XXX
> ```
>
> > Qu'est ce que l'_agent forwarding_ ? Quels sont les risques ?

### Déployer une machine virtuelle via l'interface de Scaleway

!> **🕑 Durée indicative : 20 mins**

Voir cette [documentation](../scaleway-vm/README.md).

> [!TEACHER]
> Découverte de [Scaleway](https://scaleway.com)

### Bien setup son shell (20 mins)

**Changer son shell par défaut pour `zsh` + installer [OhMyZsh](https://ohmyz.sh).**

_NB : votre VM est normalement déjà bien configurée concernant ce point. Pour votre productivité, nous avons également pré-configuré [fzf](https://github.com/junegunn/fzf), [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions) et [delta](https://github.com/dandavison/delta) (pour `git`)._

```bash
sudo apt update
sudo apt install zsh
# puis regarder https://ohmyz.sh/
```

> [!QUESTION]
> Quelles sont les différences majeures entre entre `bash` et `zsh` ?

> [!EXERCISE]
> Renseignez-vous sur l'existence de [thèmes](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes) `oh-my-zsh` et changer le thème par défaut (`robbyrussell`) pour un qui vous convient mieux.

### Passion `git`

!> **🕑 Durée indicative : 1h30+**

> [!TEACHER]
> Commençons par une introduction sur les principes de fonctionnement de `git` (structure en _DAG_, références, etc.)

> [!EXERCISE]
> Ajouter sa clef SSH sur Gitlab / Github

> [!EXERCISE]
> L'objectif ici est pour vous de prendre en main, en expérimentant et comprennant le fonctionnement, des commandes :
>
> - `git init`
> - `git status` (`gst` avec `oh-my-zsh`)
> - `git log` (`glog` avec `oh-my-zsh`)
> - `git commit -m '...'`
> - `git add file1` (vs `git add .` vs `git add -A`)
> - `git add -p`
> - `git push`
> - `git fetch`
> - `git pull`
> - `git pull --with-rebase`
> - `git checkout` (`gco` avec `oh-my-zsh`)
> - `git checkout -b`
> - `git stash` / `git stash pop`
> - `git checkout -b`
> - `git reset` / `git reset --hard`
> - `git merge` / `git merge --ff-only`
> - `git rebase` / `git rebase --interactive`
>
> Il sera important de savoir utliser (et sortir de) `vim` quand on fait un rebase interactif également [cheatsheet](https://vim.rtorr.com/).

> [!BONUS]
>
> - `git revert`
> - `git cherry-pick`
> - `git ref-log`
>
> - Savoir configurer son _gitignore_ global
> - Savoir configurer un autre éditeur que `vim` pour l'usage avec `git`
> - Savoir configurer `git` (pager custom, etc. -- voir les [_Dofiles_ de Florent Chehab par exemple](https://gitlab.com/FloChehab/tools-dotfiles/-/blob/master/symlinks_manual/gitconfig))
> - Configurer une clef OpenPGP et signer ses commits (et ajouter sa clef pgp sur Gitlab), à quoi ça sert ?
> - Mettre en place une CI sur Gitlab
> - (Si ça cruise beaucoup trop pour certains ou certaines) Faire joujou avec `docker`

> [!TEACHER]
> Dans tous les cas, nous ferons un petit topo sur la CI.

### Passion autre tooling

!> **🕑 Durée indicative : 40 mins**

> [!EXERCISE]
> Découverte des outils suivant :
>
> - `tmux`
> - `ncdu`
> - `htop`
> - `df`

## Bonnes pratiques de dev efficace

!> **🕑 Toute l'après-midi 15h30 -> 18h30**

**Objectifs pédagogiques :**

- [ ] Comprendre le caractère essentiel des structures de données, de l'analyse de la complexité en temps et en mémoire.
- [ ] Optimisation d'algorithmes selon divers procédés
- [ ] Développer sa culture générale autour de l'écosystème numérique `Python`

### Structures de données

> [!TEACHER]
>
> - JBL aura l'honneur de vous faire un rappel sur les structures de données et la complexité en temps.

### Structures de données en `Python`

> [!TEACHER]
>
> - Point sur les stuctures de données de base en python (list, dict, set) et en terme de bonnes pratiques frozenset, tuple, defaultdict, Counter
> - Passage par référence vs passage par valeur

> [!EXERCISE]
> Complexité en temps
>
> Intéressons-nous à l'exercice [`faster_is_in`](https://gitlab.utc.fr/api0057/api0057-e2022/playground/-/tree/main/lundi/faster_is_in).
> _Cet exercice peut avoir l'air simplet, mais pensez à prendre l'habitude d'écrire du code qui optimisé en temps, sinon ça ne passera pas à l'échelle._

### Mises en œuvre de calculs scientifiques efficaces

> [!EXERCISE] > [Vectorisation de code](https://gitlab.utc.fr/api0057/api0057-e2022/playground/-/tree/main/lundi/faster_cloud_diameter)
>
> La fonction présente est beaucoup trop lente et non optimisée.
>
> Objectif: faire une
> version plus rapide. Ceci est une compétition. La version la plus rapide aura
> le droit à un café.
> [Indice](https://gitlab.utc.fr/api0057/api0057-e2022/playground/-/blob/main/lundi/faster_cloud_diameter/pyproject.toml#L9)

> [!BONUS]
>
> Dans l'exercice précédent, au prix d'un résultat approximatif, pouvez-vous faire encore plus rapide ?

> [!EXERCISE]
> Complexité en mémoire (optimisation)
>
> Intéressons-nous à l'exercice [`fast_ban`](https://gitlab.utc.fr/api0057/api0057-e2022/playground/-/tree/main/lundi/fast_ban).
> _Cet exercice répond au besoin suivant : mettre au point une solution aussi rapide que possible pour estimer qu'un point appartient à un territoire (cela est formalisé dans la partie bonus de l'exercice)._

### Culture générale

> [!QUESTION]
>
> - Combien de fois peut-être plus rapide numpy, pourquoi ?
> - Qu'est-ce que le GIL en python ?
> - IO-bound VS CPU-bound

> [!TEACHER]
> Pour finir la journée, une courte intervention sur données frozen / immutables et la programmation fonctionnelle

### Bonus

> [!BONUS]
>
> - (vraiment bonus) fermeture fonctionnelle
